class Post < ApplicationRecord
	mount_uploader :image, ImageUploader
	validates :image, presence: true
	validates :title, presence: true
	validates :body, presence: true
end
